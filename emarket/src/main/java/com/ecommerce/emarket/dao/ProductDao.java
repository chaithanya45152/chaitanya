package com.ecommerce.emarket.dao;



import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ecommerce.emarket.dao.ProductDao;
import com.ecommerce.emarket.utility.ProductUtility;
import com.ecommerce.emarket.dao.UserDao;
import com.ecommerce.emarket.model.Product;
import com.ecommerce.emarket.utility.UserUtility;

public class ProductDao {
	
	public void addProducts(Product prod) {
		SessionFactory sf = ProductUtility.getSessionFactory();
		Session ss = sf.openSession();
		Transaction tr = ss.beginTransaction();
		ss.save(prod);
		tr.commit();
		ss.close();
	}
	
	public void updateProducts(Product prod) {
		SessionFactory sf = ProductUtility.getSessionFactory();
		Session ss = sf.openSession();
		Transaction tr = ss.beginTransaction();
		ss.update(prod);
		tr.commit();
		ss.close();
	}
	
	public List<Product> getAllProducts(){
		SessionFactory sf = ProductUtility.getSessionFactory();	
		Session ss = sf.openSession();
		return ss.createQuery("from Product",Product.class).getResultList();
	}
	
	public Product getProductById(long prod_Id) {
		   SessionFactory sessionFactory = ProductUtility.getSessionFactory();
		   Session session = sessionFactory.openSession();
		   return session.find(Product.class, prod_Id);
		 }
	
	public void deleteProductById(long prod_Id) {
		   Product prod = getProductById(prod_Id);
		   SessionFactory sessionFactory = ProductUtility.getSessionFactory();
		   Session session = sessionFactory.openSession();
		   Transaction transaction = session.beginTransaction();
		   session.delete(prod);
		   transaction.commit();
		   session.close();
		 }
	
}
