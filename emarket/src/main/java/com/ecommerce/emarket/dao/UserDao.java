package com.ecommerce.emarket.dao;



import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ecommerce.emarket.model.User;

import com.ecommerce.emarket.utility.UserUtility;

public class UserDao {
	
	public void addUser(User user) {
		SessionFactory sf = UserUtility.getSessionFactory();
		Session ss = sf.openSession();
		Transaction tr = ss.beginTransaction();
		ss.save(user);
		tr.commit();
		ss.close();
	}
	
	public void updateUser(User user) {
		SessionFactory sf = UserUtility.getSessionFactory();
		Session ss = sf.openSession();
		Transaction tr = ss.beginTransaction();
		ss.update(user);
		tr.commit();
		ss.close();
	}
	
	public List<User> getAllUser(){
		SessionFactory sf = UserUtility.getSessionFactory();	
		Session ss = sf.openSession();
		return ss.createQuery("from User",User.class).getResultList();
	}
	
	public User getUserById(String user_id) {
		   SessionFactory sessionFactory = UserUtility.getSessionFactory();
		   Session session = sessionFactory.openSession();
		   return session.find(User.class, user_id);
		 }
	
	public void deleteUserById(String user_Id) {
		   User user = getUserById(user_Id);
		   SessionFactory sessionFactory = UserUtility.getSessionFactory();
		   Session session = sessionFactory.openSession();
		   Transaction transaction = session.beginTransaction();
		   session.delete(user);
		   transaction.commit();
		   session.close();
		 }
	
}
